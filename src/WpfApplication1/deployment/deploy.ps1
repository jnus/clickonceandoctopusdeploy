
Import-Module .\deployment\clickOnceLib.psm1

# Properties
$baseDir = resolve-path .\
$mageExe = '.\deployment\mage.exe'
$roboCopyExe = '.\deployment\robocopy.exe'
$codeSigningCertificate = "$baseDir\deployment\MySPC.pfx"
$clickOnceApplicationName = "WpfApplication1"
$ClickOnceArtifactDir = "$baseDir\ClickOnceArtifacts"
$ClickOnceOutputDir = "$baseDir\ClickOnce"
$ClickOnceDestDir = "$WebSitePath\ClickOnce" #Octopus variable
$version = $OctopusReleaseNumber #Octopus variable

#Configuration goes here...

Write-Host "Packaging new ClickOnce version $version"
$files = Get-ChildItem -Path $ClickOnceArtifactDir | foreach {$_.fullname}
Create-ClickOncePackage $files -App_Name $clickOnceApplicationName -Version $version -Output_Dir $ClickOnceOutputDir -Cert $codeSigningCertificate -Deployment_Url "http://dev-clickonceandoctopus.vfltest.dk/clickonce" -MageExe $mageExe | Write-Host

Write-Host 'Create-ClickOncePackage $files -App_Name $clickOnceApplicationName -Version $version -Output_Dir $ClickOnceOutputDir -Cert $codeSigningCertificate -Deployment_Url "http://dev-clickonceandoctopus.vfltest.dk/clickonce" -MageExe $mageExe'


if ($lastExitCode -ne 0) {
    throw "Error: Failed to create clickonce package"
}

# prepare a click once installer from a directory
#Prepare-ClickOnce `
#    '..\installers'`						# output directory for the package
#    '1.2.3.4'`	                     		# version of the installer
#    '..\bin\Release'              		# directory to clickonce-ify                     
#    'MyApplication.exe'           		# application executable
#    'MyCompanyAwesomeApp'          	 	# your application identity name
#    'My Awesome Application'       	 	# the display name for the application
#    'icon.png'	                      	# your app icon
#    'my company'	                    # the company publishing
#    'http://mycompany.com/downloads/' 	# the download location for the installer
#    'my-certificate-thumbprint'         # a certificate thumbprint to sign the package with (optional)
	
Write-Host "Copy new ClickOnce package to website $ClickOnceDestDir"
& $roboCopyExe "$ClickOnceOutputDir" "$ClickOnceDestDir" /IS /S /PURGE /V
if ($lastExitCode -gt 4) {
	throw "Error: Failed to copy clickonce package to $ClickOnceDestDir"
}
else
{   # exit codes 1, 2, 4 are success.
	$lastExitCode = 0
}
